import React from 'react'
import { StyleSheet, Text, TouchableOpacity } from 'react-native'
import { WARNA_SEKUNDER, WARNA_UTAMA } from '../../utils/constants'
import { IconHome, IconHomeAktif, IconJadwal, IconJadwalAktif, IconUser, IconUserAktif } from "../../assets";

const TabItem = ({ label, isFocused, onLongPress, onPress }) => {

  const Icon = () => {
    if (label === "Home") {
      return isFocused ? <IconHomeAktif /> : <IconHome />
    }

    if (label === "Jadwal") {
      return isFocused ? <IconJadwalAktif /> : <IconJadwal />
    }

    if (label === "Akun") {
      return isFocused ? <IconUserAktif /> : <IconUser />
    }
  }

  return (
    <TouchableOpacity
      onPress={onPress}
      onLongPress={onLongPress}
      style={isFocused ? styles.containerFocus : styles.conteiner}
    >
      <Icon />
      {isFocused && <Text style={styles.text}>
        {label.toUpperCase()}
      </Text>}
    </TouchableOpacity>
  )
}

export default TabItem

const styles = StyleSheet.create({
  conteiner: {
    alignContent: 'center',
    padding: 5
  },
  containerFocus: {
    alignContent: 'center',
    padding: 5,
    backgroundColor: WARNA_SEKUNDER,
    flexDirection: 'row',
    borderRadius: 10
  },
  text: {
    color: WARNA_UTAMA,
    fontSize: 18,
    marginLeft: 8,
    fontFamily: 'Poppins-Bold'
  }
})
