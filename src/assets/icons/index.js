import IconHome from './home.svg'
import IconHomeAktif from './homeAktif.svg'
import IconJadwal from './jadwal.svg'
import IconJadwalAktif from './jadwalAktif.svg'
import IconUser from './user.svg'
import IconUserAktif from './userAktif.svg'
import IconDefaultUser from "./defaultUser.svg";
import IconStadion from "./stadion.svg";
import IconBooking from "./booking.svg";
import IconLangganan from "./langganan.svg";
import IconjoinTeam from "./joinTeam.svg";
export { IconHome, IconHomeAktif, IconJadwal, IconJadwalAktif, IconUser, IconUserAktif, IconDefaultUser, IconStadion, IconBooking, IconLangganan, IconjoinTeam }